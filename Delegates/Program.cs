﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintHeader();

            List<int> set = new List<int> {93, 445, 2, 5, 6, 25, 223, 3, 7, 56, 4};

            List<int> lowGroup = Filter(set, LessThanEightyTwo);

            List<int> oddGroup = Filter(set, IsOdd);

            List<int> highGroup = Filter(set, n =>
            {
                if (n > 100)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            });

            List<int> evenGroupMethod = Filter(set, IsEven);

            List<int> evenGroupLambda = Filter(set, n =>
            {
                if (n % 2 == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            });
        }

        static List<int> Filter(List<int> numbers, Predicate<int> customLogic)
        {
            List<int> resultSet = new List<int>();

            foreach (int num in numbers)
            {
                if (customLogic(num))
                {
                    resultSet.Add(num);
                }
            }

            return resultSet;
        }

        static bool IsOdd(int num)
        {
            if (num % 2 != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static bool LessThanEightyTwo(int num)
        {
            if (num < 82)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static bool IsEven(int num)
        {
            if (num % 2 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void PrintHeader()
        {
            string header = @"

______     _                  _            
|  _  \   | |                | |           
| | | |___| | ___  __ _  __ _| |_ ___  ___ 
| | | / _ \ |/ _ \/ _` |/ _` | __/ _ \/ __|
| |/ /  __/ |  __/ (_| | (_| | ||  __/\__ \
|___/ \___|_|\___|\__, |\__,_|\__\___||___/
                   __/ |                   
                  |___/                    
";

            Console.WriteLine(header);
        }
    }
}